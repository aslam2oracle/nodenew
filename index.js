/* const express = require('express');
const app = express();
const PORT = 7000;

// Middleware
app.use(express.json());

// Routes
app.get('/', (req, res) => {
  res.send('<p style="color: #874000;"> color added ,Hi, I have created multiple environments and this docker image is with snyk container test done and downloaded from my personal docker hub and deployed in local kubernetes using kustomize!</p>');
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
}); */


const express = require('express');
const app = express();
const PORT = 7000;

// Middleware
app.use(express.json());

// Routes
app.get('/', (req, res) => {
  // Existing response with additional HTML content and big fonts
  const html = `
    <div id="container" style="padding: 20px; text-align: center;">
      <h1 style="font-size: 36px;">Click Anywhere to Change Background Color</h1>
      <p style="font-size: 24px; color: #554000;">Hi, I have created multiple environments and this docker image is with snyk container test done and downloaded from my personal docker hub and deployed in local kubernetes using kustomize!</p>
      <script>
        document.addEventListener('click', function() {
          const colors = ['#cc0000', '#0000ff', '#00ff00', '#800080']; // List of colors
          const randomColor = colors[Math.floor(Math.random() * colors.length)]; // Select a random color
          document.body.style.backgroundColor = randomColor; // Set the background color
        });
      </script>
    </div>
  `;
  // Send the modified HTML response
  res.send(html);
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});